/*
 * Copyright (c) 2018 Final Elite. All rights reserved.
 *
 * This file can not be copied and/or distributed without the express permission of the Final Elite Staff.
 */

package br.com.finalelite.trident.utils;

import br.com.finalelite.trident.item.CustomTrident;
import br.com.finalelite.trident.item.TridentAction;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

public class TridentManager {

    private Map<String, CustomTrident> tridents = new HashMap<>();

    public void registerCustomTrident(CustomTrident trident) {
        if (tridents.containsKey(trident.getName()))
            throw new InvalidParameterException("Trident already registred");

        tridents.put(trident.getColouredName(), trident);
    }

    public boolean isValid(String name) {
        return tridents.containsKey(name);
    }

    public TridentAction getAction(String name) {
        if (!isValid(name))
            return null;
        return tridents.get(name).getAction();
    }

    public CustomTrident getTrident(String name) {
        name = CustomTrident.colour(name);
        if (!isValid(name))
            return null;
        return tridents.get(name);
    }

    public Map<String, CustomTrident> getTridents() {
        return new HashMap<>(tridents);
    }

}
