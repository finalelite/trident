/*
 * Copyright (c) 2018 Final Elite. All rights reserved.
 *
 * This file can not be copied and/or distributed without the express permission of the Final Elite Staff.
 */

package br.com.finalelite.trident.item;

import br.com.finalelite.trident.Main;
import lombok.Getter;
import lombok.val;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class CustomTrident {

    @Getter
    private final String name;
    @Getter
    private final String colouredName;
    @Getter
    private final String description;
    @Getter
    private final TridentAction action;

    private CustomTrident(String name, String description, TridentAction action) {
        this.name = name;
        this.colouredName = colour(name);
        this.description = description;
        this.action = action;

        Main.getTridentManager().registerCustomTrident(this);
    }

    public static CustomTrident createCustomTrident(String name, String description, TridentAction action) {
        return new CustomTrident(name, description, action);
    }

    public static String colour(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public ItemStack getItemStack() {
        val item = new ItemStack(Material.TRIDENT);
        val meta = item.getItemMeta();

        meta.setDisplayName(colour(name));
        val descriptionArray = colour(description).split("\n");

        meta.setLore(Arrays.asList(descriptionArray));

        item.setItemMeta(meta);
        return item;
    }
}
