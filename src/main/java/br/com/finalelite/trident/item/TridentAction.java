/*
 * Copyright (c) 2018 Final Elite. All rights reserved.
 *
 * This file can not be copied and/or distributed without the express permission of the Final Elite Staff.
 */

package br.com.finalelite.trident.item;

import org.bukkit.entity.Player;
import org.bukkit.entity.Trident;

@FunctionalInterface
public interface TridentAction {

    void action(Player shooter, Player victim, Trident trident);

}
