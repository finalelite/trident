/*
 * Copyright (c) 2018 Final Elite. All rights reserved.
 *
 * This file can not be copied and/or distributed without the express permission of the Final Elite Staff.
 */

package br.com.finalelite.trident.listeners;

import br.com.finalelite.trident.Main;
import lombok.val;
import lombok.var;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Trident;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EntityListener implements Listener {

    private Map<String, ItemStack> items = new HashMap<>();
    private Map<Player, ItemStack> lastTrident = new HashMap<>();

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        val damager = event.getDamager();
        val victim = event.getEntity();

        // Check if the victim is a Player
        if (!(victim.getType() == EntityType.PLAYER))
            return;

        val victimPlayer = (Player) victim;

        // Check if the damager is a trident
        if (damager.getType() != EntityType.TRIDENT)
            return;

        val trident = (Trident) damager;
        
        // check if the shooter is a player 
        if(!(trident.getShooter() instanceof Player))
            return;

        val shooter = (Player) trident.getShooter();

        if (!trident.hasMetadata("tridentName"))
            return;

        val name = trident.getMetadata("tridentName").get(0).asString();
        val action = Main.getTridentManager().getAction(name);
        if (action != null)
            action.action(shooter, victimPlayer, trident);
    }

    @EventHandler
    public void onHit(ProjectileHitEvent event) {
        val entity = event.getEntity();
        if (entity.getType() == EntityType.TRIDENT) {
            if (!entity.hasMetadata("tridentItemUUID") || !(entity.getShooter() instanceof Player))
                return;

            val uuid = entity.getMetadata("tridentItemUUID").get(0).asString();
            var item = items.get(uuid);
            val meta = item.getItemMeta();
            item.setItemMeta(meta);
            val player = (Player) entity.getShooter();

            if (player.getInventory().firstEmpty() != -1) {
                player.getInventory().addItem(item);
            } else {
                player.getLocation().getWorld().dropItemNaturally(player.getLocation(), item);
            }

            items.remove(uuid);

            entity.remove();
        }
    }

    @EventHandler
    public void onThrowTrident(ProjectileLaunchEvent event) {
        val projectile = event.getEntity();
        if (!(projectile.getShooter() instanceof Player))
            return;

        val player = (Player) event.getEntity().getShooter();
        val item = lastTrident.get(player);

        if (item.getType() != Material.TRIDENT)
            return;

        if (!projectile.hasMetadata("tridentItemUUID")) {
            if (player.getGameMode() != GameMode.CREATIVE) {
                val uuid = addReturningUUID(item);
                projectile.setMetadata("tridentItemUUID", new FixedMetadataValue(Main.getInstance(), uuid));
            }
        }

        if (!item.hasItemMeta() || item.getItemMeta().getLore() == null)
            return;

        val name = item.getItemMeta().getDisplayName();
        projectile.setMetadata("tridentName", new FixedMetadataValue(Main.getInstance(), name));
    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event) {
        if (event.getHand() == EquipmentSlot.HAND)
            lastTrident.put(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand());
        else if (event.getHand() == EquipmentSlot.OFF_HAND)
            lastTrident.put(event.getPlayer(), event.getPlayer().getInventory().getItemInOffHand());

    }

    @EventHandler
    public void onExit(PlayerQuitEvent event) {
        val player = event.getPlayer();
        player.setWalkSpeed(0.2f);
        player.setFlySpeed(0.1f);
        lastTrident.remove(event.getPlayer());
    }

    @EventHandler
    public void onJump(PlayerMoveEvent event) {
        val from = event.getFrom();
        val to = event.getTo();
        val player = event.getPlayer();

        if (player.getWalkSpeed() != 0.0)
            return;

        if (to.getY() > from.getY())
            event.setCancelled(true);
    }

    private String addReturningUUID(ItemStack item) {
        UUID uuid;
        do {
            uuid = UUID.randomUUID();
        } while (items.containsKey(uuid.toString()));

        items.put(uuid.toString(), item);
        return uuid.toString();
    }

}
