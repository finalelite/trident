/*
 * Copyright (c) 2018 Final Elite. All rights reserved.
 *
 * This file can not be copied and/or distributed without the express permission of the Final Elite Staff.
 */

package br.com.finalelite.trident;

import br.com.finalelite.trident.item.CustomTrident;
import br.com.finalelite.trident.listeners.EntityListener;
import br.com.finalelite.trident.utils.TridentManager;
import lombok.Getter;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Main extends JavaPlugin {

    @Getter
    private static TridentManager tridentManager;
    @Getter
    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;
        tridentManager = new TridentManager();

        // Register events
        getServer().getPluginManager().registerEvents(new EntityListener(), this);

        // Register tridents
        registerDefaultTridents();

        // Register a test command
        getServer().getPluginCommand("trident").setExecutor((commandSender, command, s, strings) -> {
            val player = (Player) commandSender;
            if (!player.isOp()) {
                commandSender.sendMessage(ChatColor.RED + "Sem permissão.");
                return true;
            }

            if (strings.length == 0)
                return false;

            if (strings[0].equals("*")) {
                tridentManager.getTridents().keySet().forEach(t -> player.getInventory().addItem(tridentManager.getTrident(t).getItemStack()));
                return true;
            }

            int i = 0;
            try {
                i = Integer.parseInt(strings[0]);
                if (i > 5 || i < 0)
                    return false;
            } catch (NumberFormatException e) {
                return false;
            }

            val trident = tridentManager.getTrident(tridentManager.getTridents().keySet().toArray(new String[0])[i]);
            player.getInventory().addItem(trident.getItemStack());

            return true;
        });
    }

    private void registerDefaultTridents() {
        CustomTrident.createCustomTrident("&7Tridente Sequestrador", "\n&aPuxe o jogador até você",
                (shooter, victim, trident) -> {
                    victim.teleport(shooter);
                });

        CustomTrident.createCustomTrident("&6Tridente Flamejante", "\n&aColoque fogo no jogador",
                (shooter, victim, trident) -> victim.setFireTicks(120));

        CustomTrident.createCustomTrident("&bTridente Congelante", "\n&aTrave o jogador por 3 segundos",
                (shooter, victim, trident) -> {
                    victim.setWalkSpeed(0);
                    victim.setFlySpeed(0);
                    Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(), () -> {
                        if (victim.isOnline()) {
                            victim.setWalkSpeed(0.2f);
                            victim.setFlySpeed(0.1f);
                        }
                    }, 60);
                });

        CustomTrident.createCustomTrident("&2Tridente Envenenado", "\n&aEnvenene o jogador por 10 segundos",
                (shooter, victim, trident) -> victim.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20 * 10, 2)));

        CustomTrident.createCustomTrident("&8Tridente da Lentidão", "\n&aDeixe o jogador lento por 10 segundos",
                (shooter, victim, trident) -> {
                    victim.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * 10, 2));
                });

        CustomTrident.createCustomTrident("&1Tridente Elétrico", "\n&aJogue 2 raios no jogador",
                (shooter, victim, trident) -> {
                    victim.getLocation().getWorld().strikeLightning(victim.getLocation());

                    Bukkit.getServer().getScheduler().runTaskLater(Main.getInstance(),
                            () -> {
                                if (victim.isOnline()) {
                                    victim.getLocation().getWorld().strikeLightning(victim.getLocation());
                                }
                            }, 40);
                });

    }

}
