# Trident

Plugin que adiciona tridentes com efeitos especiais.

**Ideia: Willzy**  
**Autor do código: Pauloo27**

## Lista de tridentes
- Tridente do Congelamento (Congela o player durante 3 segundos)
- Tridente Incendiário (Coloca fogo no jogador)
- Tridente de Envenenamento (Envenena o jogador)
- Tridente do Sequestro (Pucha o jogador para o atacante)
- Tridente da Lentidão (Deixa o jogador lentro)
- Tridente de Raio (Cai raio 2 vezes no player em intervalo de 2 segundos)

## Como usar
Usando a API do plugin é possivel adicionar e pegar tridentes.

### Criando e registrando um tridente

Abaixo, um exemplo de tridente que coloca fogo na vitima. Com isso, o tridente será "automágicamente" registrado.
```java
// Nome, Descrisão, Ação
CustomTrident.createCustomTrident("Incendiário", "&aColoca fogo no jogador", (shooter, victim, trident) -> {
    victim.setFireTicks(120);
});
```

### Pegando o ItemStack

Com o código abaixo, é possível pegar um ItemStack que representará o trident. O nome dever ser de um tridente já registrado.
```java
Main.getTridentManager().getTrident("Incendiário").getItemStack();
```